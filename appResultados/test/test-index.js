const server = require("../index")
const chai = require("chai")
const chaiHttp = require("chai-http")
const { resolveContent } = require("nodemailer/lib/shared")

chai.should()
chai.use(chaiHttp)

describe("task API",()=>{

    describe("GET api/elecciones",()=>{

            it("Tarea tipo array",(done)=>{
                chai.request(server).get("/api/elecciones").end((error,response)=>{
                    response.body.should.be.a('array');
                        done()
                })
            })
            it("Tarea status 200",(done)=>{
                chai.request(server).get("/api/elecciones").end((error,response)=>{
                    response.should.have.status(200);
                        done()
                })
            })

    } )


    describe("GET api/Auditoria",()=>{

        it("Tarea status 200",(done)=>{
            chai.request(server).get("/api/auditoria").end((error,response)=>{
                response.should.have.status(200);
                    done()
            })
        })
        it("Tarea tipo json",(done)=>{
            chai.request(server).get("/api/auditoria").end((error,response)=>{
                response.should.to.be.json;
                    done()
            })
        })

} )





}

)

