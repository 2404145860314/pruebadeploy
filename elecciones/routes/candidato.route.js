var express = require('express')
var candidato = express.Router();
var controller = require('../controllers/candidato.controller');

candidato.get('/candidato/get_all/:id', controller.default.getInstance().get_one);
candidato.post('/candidato/create', controller.default.getInstance().create);
module.exports = candidato;